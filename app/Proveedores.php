<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Proveedores extends Model
{
    protected $table = 'proveedores';

    protected $fillable = [
        'nombre','rfc', 'email','user_id', 'estatus_id'
    ];

     public function user()
    {
        return $this->belongsTo(User::class, 'user_id');
    }
    
    public function status()
    {
        return $this->belongsTo(Estatus::class, 'estatus_id');
    }
}
