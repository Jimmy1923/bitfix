<?php

namespace App\Imports;

use App\Proveedores;
use Maatwebsite\Excel\Concerns\ToModel;

class ProveedoresImport implements ToModel
{
    /**
    * @param array $row
    *
    * @return \Illuminate\Database\Eloquent\Model|null
    */
    public function model(array $row)
    {
        return new Proveedores([
            'nombre'   => $row[0], //a
            'rfc'   => $row[1], //b
            'email'   => $row[2], //c
        ]);
    }
}
