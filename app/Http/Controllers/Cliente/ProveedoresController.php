<?php

namespace App\Http\Controllers\Cliente;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Maatwebsite\Excel\Facades\Excel;
use App\Imports\ProveedoresImport;
use App\Proveedores;
use DB;
use DataTables;

class ProveedoresController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function proveedors(Request $request)
    {
         if ($request->ajax()) {
            $data = Proveedores::where('user_id', auth()->user()->id)->where('estatus_id', 1)->get();
             return Datatables::of($data)
                    ->editColumn('status', function($proveedor)
                          {
                             return $proveedor->status->nombre;
                          })
                        ->editColumn('user', function($proveedor)
                        {
                            return $proveedor->user->name;
                        })
                    ->addIndexColumn()
                    ->addColumn('btn', 'clientes.opciones')
                    ->rawColumns(['btn'])
                    ->make(true);
         }
    }

    public function proveedoresAutorizado(Request $request)
    {
         if ($request->ajax()) {
            $data = Proveedores::where('user_id', auth()->user()->id)->where('estatus_id', 2)->get();
             return Datatables::of($data)
                    ->editColumn('status', function($proveedor)
                          {
                             return $proveedor->status->nombre;
                          })
                        ->editColumn('user', function($proveedor)
                        {
                            return $proveedor->user->name;
                        })
                    ->addIndexColumn()
                    ->make(true);
         }
    }

    public function proveedoresRechazado(Request $request)
    {
         if ($request->ajax()) {
            $data = Proveedores::where('user_id', auth()->user()->id)->where('estatus_id', 3)->get();
             return Datatables::of($data)
                    ->editColumn('status', function($proveedor)
                          {
                             return $proveedor->status->nombre;
                          })
                        ->editColumn('user', function($proveedor)
                        {
                            return $proveedor->user->name;
                        })
                    ->addIndexColumn()
                    ->make(true);
         }
    }

    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'file' => 'required|file|max:5000|mimes:xls,csv,xlsx'
        ]);
        $file = $request->file('file');
        $provs = Excel::toCollection(new ProveedoresImport, $file);
            foreach ($provs[0] as $prov) {
                $existencia = DB::table('proveedores')
                ->select('nombre')
                ->where('nombre', '=', $prov[0])
                ->get();

                if ($prov[0] != null) {
                    if(count($existencia) == 0){
                        $proveedor = Proveedores::create([
                            'nombre' => $prov[0],
                            'rfc' => $prov[1],
                            'email' => $prov[2],
                            'estatus_id' => 1,
                            'user_id' => auth()->user()->id,
                        ]);
                        /*
                        $usuario = User::create([
                            'name' => $proveedor->nombre,
                            'email' => $proveedor->email,
                            'password' => bcrypt(12345678),
                            'role_id' => 3,
                        ]);
                        */
                    }
                }
            }
        alert()->success('Importanción de proveedores completada.', 'Proveedores exportados');
        return back();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request,[
            'nombre' => 'required',
            'rfc' => 'required',
            'email' => 'required',
        ]);

        $proveedor = Proveedores::find($id)->update($request->except(['_method', '_token']));
        alert()->success('El proveedor ha sido actualizado correctamente.', 'Proveedor actualizado');
        return redirect()->route('clientes.dashboard');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
