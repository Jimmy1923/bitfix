<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\User;
use App\Roles;
use DataTables;

class UsuarioController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $rol = Roles::all();
        return view('admin.usuario.index', compact('rol'));
    }

    public function listUser(Request $request)
    {
        if ($request->ajax()) {
            $data = User::all();
            return Datatables::of($data)
                ->editColumn('role', function ($user) {
                    return $user->role->nombre;
                })
                ->editColumn('proveedores', function($user){
                    return $user->proveedores->count();
                 })             
                ->addIndexColumn()
                ->addColumn('btn', 'admin.usuario.opciones')
                ->rawColumns(['btn'])
                ->make(true);
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'name' => 'required',
            'email' => 'required|unique:users,email',
            'password' => 'required',
            'role_id' => 'required',
        ]
    );
   $usuario = User::create([
        'name' => $request->get('name'),
        'ap_paterno' => $request->get('ap_paterno'),
        'ap_materno' => $request->get('ap_materno'),
        'email' => $request->get('email'),
        'password' => bcrypt($request->get('password')),
        'role_id' => $request->get('role_id'),
    ]);

    alert()->success('El usuario ha sido creado correctamente.', 'Usuario creado');
    return redirect()->route('usuarios.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request,[
            'name' => 'required',
            'ap_paterno' => 'required',
            'ap_materno' => 'required',
            'email' => 'required',
            'role_id' => 'required'
        ]);
        $usuario = User::find($id)->update($request->except(['_method', '_token']));
        alert()->success('El usuario ha sido actualizado correctamente.', 'Usuario actualizada');
        return redirect()->route('usuarios.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
