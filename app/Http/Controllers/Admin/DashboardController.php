<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Proveedores;
use App\User;
use DataTables;

class DashboardController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $provPend = Proveedores::where('estatus_id', 1)->get();
        $provAut = Proveedores::where('estatus_id', 2)->get();
        $provRech = Proveedores::where('estatus_id', 3)->get();
        return view('admin.proveedores.index', compact('provPend', 'provAut', 'provRech'));
    }

    public function proveedoresAdminPend(Request $request)
    {
        if ($request->ajax()) {
            $data = Proveedores::where('estatus_id', 1)->get();
            return Datatables::of($data)
                ->editColumn('status', function ($proveedor) {
                    return $proveedor->status->nombre;
                })
                ->editColumn('user', function ($proveedor) {
                    return $proveedor->user->name;
                })
                ->addIndexColumn()
                ->addColumn('btn', 'admin.proveedores.opciones')
                ->rawColumns(['btn'])
                ->make(true);
        }
    }

    public function proveedoresAdminAutorizado(Request $request)
    {
        if ($request->ajax()) {
            $data = Proveedores::where('estatus_id', 2)->get();
            return Datatables::of($data)
                ->editColumn('status', function ($proveedor) {
                    return $proveedor->status->nombre;
                })
                ->editColumn('user', function ($proveedor) {
                    return $proveedor->user->name;
                })
                ->addIndexColumn()
                ->make(true);
        }
    }

    public function proveedoresAdminRechazado(Request $request)
    {
        if ($request->ajax()) {
            $data = Proveedores::where('estatus_id', 3)->get();
            return Datatables::of($data)
                ->editColumn('status', function ($proveedor) {
                    return $proveedor->status->nombre;
                })
                ->editColumn('user', function ($proveedor) {
                    return $proveedor->user->name;
                })
                ->addIndexColumn()
                ->make(true);
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request,[
            'nombre' => 'required',
            'rfc' => 'required',
            'email' => 'required',
            'estatus_id' => 'required',
        ]);

        $proveedor = Proveedores::findOrFail($id);
            $proveedor->estatus_id = $request->estatus_id;
        $proveedor->save();

        if ($request->estatus_id == 2) {
            $usuario = User::create([
                'name' => $proveedor->nombre,
                'email' => $proveedor->email,
                'password' => bcrypt(12345678),
                'role_id' => 3,
            ]);
        } 
        alert()->success('El proveedor ha sido actualizado correctamente.', 'Proveedor actualizado');
        return redirect()->route('admin.dashboard');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
