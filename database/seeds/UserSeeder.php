<?php

use Illuminate\Database\Seeder;
use App\User;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $user = new User();
            $user->name = "Administrador";
            $user->ap_paterno = "";
            $user->ap_materno = "";
            $user->email = "admin@admin.com";
            $user->password =  bcrypt(12345678);
            $user->role_id = 1;
        $user->save();        

        $user = new User();
            $user->name = "Cliente";
            $user->ap_paterno = "";
            $user->ap_materno = "";
            $user->email = "cliente@cliente.com";
            $user->password =  bcrypt(12345678);
            $user->role_id = 2;
        $user->save();  

    }
}
