<?php

use Illuminate\Database\Seeder;
use App\Estatus;

class EstatusSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        {
            $status = new Estatus();
                $status->nombre = "Pendiente";
            $status->save();        
    
            $status = new Estatus();
                $status->nombre = "Autorizado";
            $status->save();      
    
            $status = new Estatus();
                $status->nombre = "Cancelado";
            $status->save();
        }
    }
}
