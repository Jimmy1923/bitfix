<?php

use Illuminate\Database\Seeder;
use App\Roles;

class RolesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $rol = new Roles();
            $rol->nombre = "Administrador";
        $rol->save();        

        $rol = new Roles();
            $rol->nombre = "Cliente";
        $rol->save();

        $rol = new Roles();
            $rol->nombre = "Proveedores";
        $rol->save();
    }
}
