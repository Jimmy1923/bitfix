## Ejecucion de la prueba

Se clona el repositorio.

1. Nos posicionamos en la ruta del sistema y tiramos un **composer install.**
2. Creamos una base de datos, y colocamos en el **.env** el nombre de la base de datos, el usuario y contraseña.
3. En terminal corremos el siguiente comando **php artisan migrate --seed** para crear las tablas y registros.
4. Se ejecuta el sistema con un **php artisan serve**
5. Las credenciales para el login es para el **Administrador(correo: admin@admin.com  contraseña: 12345678)**
6. Las credenciales para el login es para el **Cliente(correo: cliente@cliente.com  contraseña: 12345678)**
---