<?php

Route::group(['middleware' => ['auth','role:2'], 'prefix' => 'clientes'], function(){
    Route::get('dashboard', 'Cliente\DashboardController@index')->name('clientes.dashboard');
    Route::resource('proveedores', 'Cliente\ProveedoresController');
    Route::get('proveedors', 'Cliente\ProveedoresController@proveedors')->name('clientes.proveedors');
    Route::get('proveedoresAutorizado', 'Cliente\ProveedoresController@proveedoresAutorizado')->name('clientes.proveedoresAutorizado');
    Route::get('proveedoresRechazado', 'Cliente\ProveedoresController@proveedoresRechazado')->name('clientes.proveedoresRechazado');
});
