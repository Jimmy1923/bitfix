<?php

Route::group(['middleware' => ['auth','role:1'], 'prefix' => 'admin'], function(){
    Route::get('dashboard', 'Admin\DashboardController@index')->name('admin.dashboard');
    Route::resource('proveedoresAdmin', 'Admin\DashboardController');
    Route::get('proveedoresAdminPend', 'Admin\DashboardController@proveedoresAdminPend')->name('admin.proveedoresAdminPend');
    Route::get('proveedoresAdminAutorizado', 'Admin\DashboardController@proveedoresAdminAutorizado')->name('admin.proveedoresAdminAutorizado');
    Route::get('proveedoresAdminRechazado', 'Admin\DashboardController@proveedoresAdminRechazado')->name('admin.proveedoresAdminRechazado');
    Route::resource('usuarios', 'Admin\UsuarioController');
    Route::get('listUser', 'Admin\UsuarioController@listUser')->name('admin.listUser');
});
