<div class="mobile-menu md:hidden">
    <div class="mobile-menu-bar">
        <a href="/home" class="flex mr-auto">
            <img class="w-6" src="{{ asset('images/logo.svg')}}">
        </a>
        <a href="javascript:;" id="mobile-menu-toggler"> <i data-feather="bar-chart-2" class="w-8 h-8 text-white transform -rotate-90"></i> </a>
    </div>
    <ul class="border-t border-theme-24 py-5 hidden">
        <li>
            <a href="/admin/dashboard" class="{{ (request()->is('admin/dashboard')) ? 'menu menu--active' : 'menu' }}">
                <div class="menu__icon"> <i data-feather="home"></i> </div>
                <div class="menu__title"> Inicio </div>
            </a>
        </li>
        <li>
            <a href="/admin/usuarios" class="{{ (request()->is('admin/usuarios')) ? 'menu menu--active' : 'menu' }}">
                <div class="menu__icon"> <i data-feather="users"></i> </div>
                <div class="menu__title"> Centro de trabajo </div>
            </a>
        </li>
    </ul>
</div>
<!-- END: Mobile Menu -->
<div class="flex">
    <!-- BEGIN: Side Menu -->
    <nav class="side-nav">
        <a href="/home" class="intro-x flex items-center pl-5 pt-4">
            <img alt="Siap" class="w-6" src="{{ asset('images/logo.svg')}}">
            <span class="hidden xl:block text-white text-lg ml-3"> SI<span class="font-medium">AP</span> </span>
        </a>
        <div class="side-nav__devider my-6"></div>
        <ul>
            <li>
                <a href="/admin/dashboard" class="{{ (request()->is('admin/dashboard')) ? 'side-menu side-menu--active' : 'side-menu' }}">
                    <div class="side-menu__icon"> <i data-feather="home"></i> </div>
                    <div class="side-menu__title"> Inicio </div>
                </a>
            </li>
            <li>
                <a href="/admin/usuarios" class="{{ (request()->is('admin/usuarios')) ? 'side-menu side-menu--active' : 'side-menu' }}">
                    <div class="side-menu__icon"> <i data-feather="users"></i> </div>
                    <div class="side-menu__title"> Usuarios </div>
                </a>
            </li>
        </ul>
    </nav>
