@extends('layouts/app')
@section('title', 'Cliente')
@section('content')

<div class="intro-y flex flex-col sm:flex-row items-center mt-8">
    <h2 class="text-lg font-medium mr-auto">
        Proveedores
    </h2>
    <div class="w-full sm:w-auto flex mt-4 sm:mt-0">
        <a class="button text-white bg-theme-1 shadow-md mr-2" href="javascript:;" data-toggle="modal" data-target="#crear_proveedores" >Importar proveedores</a>
        @include('clientes.proveedores.create')
    </div>
</div><br><br>
<div class="intro-y pr-1">
    <div class="box p-2">
        <div class="chat__tabs nav-tabs justify-center flex">
            <a data-toggle="tab" data-target="#pendientes" href="javascript:;" class="flex-1 py-2 rounded-md text-center active">Proveedores pendientes - {{$provPend->count()}}</a>
            <a data-toggle="tab" data-target="#autorizados" href="javascript:;" class="flex-1 py-2 rounded-md text-center">Proveedores autorizados - {{$provAut->count()}}</a>
            <a data-toggle="tab" data-target="#rechazados" href="javascript:;" class="flex-1 py-2 rounded-md text-center">Proveedores rechazados - {{$provRech->count()}}</a>
        </div>
    </div>
</div>
<div class="tab-content">
<br>
 <div class="tab-content__pane active" id="pendientes">
	<div class="intro-y datatable-wrapper overflow-x-auto">
	    <table class="table table-report table-report--bordered display datatable w-full" id="proveedores">
	        <thead>
	            <tr>
	                <th class="border-b-2 whitespace-no-wrap">Id</th>
                    <th class="border-b-2 whitespace-no-wrap">Nombre</th>
	                <th class="border-b-2 text-center whitespace-no-wrap">RFC</th>
	                <th class="border-b-2 text-center whitespace-no-wrap">Email</th>
	                <th class="border-b-2 text-center whitespace-no-wrap">Estatus</th>
	                <th class="border-b-2 text-center whitespace-no-wrap">Creador</th>
	                <th class="border-b-2 text-center whitespace-no-wrap">Opciones</th>
	            </tr>
	        </thead>
	        <tbody>

	        </tbody>
	    </table>
	</div>
</div>
<div class="tab-content__pane" id="autorizados">
	<div class="intro-y datatable-wrapper overflow-x-auto">
	    <table class="table table-report table-report--bordered display datatable w-full" id="proveedoresAutorizado">
	        <thead>
	            <tr>
	                <th class="border-b-2 whitespace-no-wrap">Id</th>
                    <th class="border-b-2 whitespace-no-wrap">Nombre</th>
	                <th class="border-b-2 text-center whitespace-no-wrap">RFC</th>
	                <th class="border-b-2 text-center whitespace-no-wrap">Email</th>
	                <th class="border-b-2 text-center whitespace-no-wrap">Estatus</th>
	                <th class="border-b-2 text-center whitespace-no-wrap">Creador</th>
	            </tr>
	        </thead>
	        <tbody>

	        </tbody>
	    </table>
	</div>
</div>
<div class="tab-content__pane" id="rechazados">
	<div class="intro-y datatable-wrapper overflow-x-auto">
	    <table class="table table-report table-report--bordered display datatable w-full" id="proveedoresRechazado">
	        <thead>
	            <tr>
	                <th class="border-b-2 whitespace-no-wrap">Id</th>
                    <th class="border-b-2 whitespace-no-wrap">Nombre</th>
	                <th class="border-b-2 text-center whitespace-no-wrap">RFC</th>
	                <th class="border-b-2 text-center whitespace-no-wrap">Email</th>
	                <th class="border-b-2 text-center whitespace-no-wrap">Estatus</th>
	                <th class="border-b-2 text-center whitespace-no-wrap">Creador</th>
	            </tr>
	        </thead>
	        <tbody>

	        </tbody>
	    </table>
	</div>
</div>
@endsection
@section('scripts_ready')
<script type="text/javascript">
    $(function () {
      $('#proveedores').DataTable().destroy();
      var table = $('#proveedores').DataTable({
          processing: true,
          serverSide: true,
        ajax: "{{ route('clientes.proveedors') }}",
        columns: [
            {data: 'id', name: 'id'},
            {data: 'nombre', name: 'nombre'},
            {data: 'rfc', name: 'rfc',class: "text-center border-b"},
            {data: 'email', name: 'email', class: "text-center border-b"},
            {data: 'status', name: 'status', class: "text-center border-b"},
            {data: 'user', name: 'user', class: "text-center border-b"},
            {data: 'btn', name: 'btn', class: "text-center border-b"},
        ],
            "language": {
                "info": "_TOTAL_ registros",
                "search": "Buscar",
                "paginate": {
                    "next": "Siguiente",
                    "previous": "Anterior",
                },
                "lengthMenu": 'Mostrar <select >'+
                            '<option value="10">10</option>'+
                            '<option value="30">30</option>'+
                            '<option value="-1">Todos</option>'+
                            '</select> registros',
                "loadingRecords": "Cargando...",
                "processing": "Procesando...",
                "emptyTable": "No hay datos",
                "zeroRecords": "No hay coincidencias",
                "infoEmpty": "",
                "infoFiltered": ""
            }
    });
  });

  $(function () {
      $('#proveedoresAutorizado').DataTable().destroy();
      var table = $('#proveedoresAutorizado').DataTable({
          processing: true,
          serverSide: true,
        ajax: "{{ route('clientes.proveedoresAutorizado') }}",
        columns: [
            {data: 'id', name: 'id'},
            {data: 'nombre', name: 'nombre'},
            {data: 'rfc', name: 'rfc',class: "text-center border-b"},
            {data: 'email', name: 'email', class: "text-center border-b"},
            {data: 'status', name: 'status', class: "text-center border-b"},
            {data: 'user', name: 'user', class: "text-center border-b"},
        ],
            "language": {
                "info": "_TOTAL_ registros",
                "search": "Buscar",
                "paginate": {
                    "next": "Siguiente",
                    "previous": "Anterior",
                },
                "lengthMenu": 'Mostrar <select >'+
                            '<option value="10">10</option>'+
                            '<option value="30">30</option>'+
                            '<option value="-1">Todos</option>'+
                            '</select> registros',
                "loadingRecords": "Cargando...",
                "processing": "Procesando...",
                "emptyTable": "No hay datos",
                "zeroRecords": "No hay coincidencias",
                "infoEmpty": "",
                "infoFiltered": ""
            }
    });
  });

  $(function () {
      $('#proveedoresRechazado').DataTable().destroy();
      var table = $('#proveedoresRechazado').DataTable({
          processing: true,
          serverSide: true,
        ajax: "{{ route('clientes.proveedoresRechazado') }}",
        columns: [
            {data: 'id', name: 'id'},
            {data: 'nombre', name: 'nombre'},
            {data: 'rfc', name: 'rfc',class: "text-center border-b"},
            {data: 'email', name: 'email', class: "text-center border-b"},
            {data: 'status', name: 'status', class: "text-center border-b"},
            {data: 'user', name: 'user', class: "text-center border-b"},
        ],
            "language": {
                "info": "_TOTAL_ registros",
                "search": "Buscar",
                "paginate": {
                    "next": "Siguiente",
                    "previous": "Anterior",
                },
                "lengthMenu": 'Mostrar <select >'+
                            '<option value="10">10</option>'+
                            '<option value="30">30</option>'+
                            '<option value="-1">Todos</option>'+
                            '</select> registros',
                "loadingRecords": "Cargando...",
                "processing": "Procesando...",
                "emptyTable": "No hay datos",
                "zeroRecords": "No hay coincidencias",
                "infoEmpty": "",
                "infoFiltered": ""
            }
    });
  });
</script>
@endsection
