<div class="modal" id="crear_proveedores">
    <div class="modal__content modal__content--xl">
        <div class="flex items-center px-5 py-5 sm:py-3 border-b border-gray-200">
            <h2 class="font-medium text-base mr-auto">
                Importar proveedores
            </h2>
        </div>
        <form method="POST" action="{{ route('proveedores.store') }}" role="form" accept-charset="UTF-8" enctype="multipart/form-data">
            {{ csrf_field() }}
        <div class="p-5 grid grid-cols-12 gap-4 row-gap-3">
            <div class="col-span-12 sm:col-span-12 @if ($errors->has('file')) has-error @endif">
                <label class="font-medium text-base mr-auto">Importar archivo:</label>
                <input type="file" class="input w-full border mt-2 flex-1" name="file" value="{{old('file')}}" aria-describedby="helpFile">
                <span id="helpFile" class="help-block">
                    @if ($errors->has('file'))
                        @foreach($errors->get('file') as $message)
                            @if(!$loop->first) / @endif
                            {{ $message }}
                        @endforeach
                    @endif
                </span>
            </div>
            <div class="col-span-12 sm:col-span-12">
            </div>
        </div>
        <div class="px-5 py-3 text-right border-t border-gray-200">
            <button type="button" data-dismiss="modal" class="button w-20 border text-gray-700 mr-1">Cancelar</button>
            <button class="button w-20 bg-theme-1 text-white">Guardar</button>
        </div>
        </form>

    </div>
</div>

