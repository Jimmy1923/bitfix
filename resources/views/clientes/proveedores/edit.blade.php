<div class="modal" id="edit-proveedor{{$id}}">
    <div class="modal__content modal__content--xl">
        <div class="flex items-center px-5 py-5 sm:py-3 border-b border-gray-200">
            <h2 class="font-medium text-base mr-auto">
                Edicion del producto: {{$nombre}}
            </h2>
        </div>
        {!! Form::open(['route' => ['proveedores.update', $id], 'method' => 'PUT']) !!}
        {{ csrf_field() }}
        <input type="hidden" name="_method" value="PUT">
        <div class="p-5 grid grid-cols-12 gap-4 row-gap-3">
            <div class="col-span-12 sm:col-span-4 @if ($errors->has('nombre')) has-error @endif">
                <label class="font-medium text-base mr-auto" for="nombre">Nombre:</label>
                <input type="text" class="input w-full border mt-2 flex-1" name="nombre" id="nombre" value="{{$nombre}}" aria-describedby="helpnombre" required>
                <span id="helpnombre" class="help-block">
                    @if ($errors->has('nombre'))
                        @foreach($errors->get('nombre') as $message)
                            @if(!$loop->first) / @endif
                            {{ $message }}
                        @endforeach
                    @endif
                </span>
            </div>
            <div class="col-span-12 sm:col-span-4 @if ($errors->has('rfc')) has-error @endif">
                <label class="font-medium text-base mr-auto" for="rfc">RFC:</label>
                <input type="text" class="input w-full border mt-2 flex-1" name="rfc" id="rfc" aria-describedby="helprfc" value="{{$rfc}}" required>
                <span id="helprfc" class="help-block">
                    @if ($errors->has('rfc'))
                        @foreach($errors->get('rfc') as $message)
                            @if(!$loop->first) / @endif
                            {{ $message }}
                        @endforeach
                    @endif
                </span>
            </div>
            <div class="col-span-12 sm:col-span-4 @if ($errors->has('email')) has-error @endif">
                <label class="font-medium text-base mr-auto" for="email">email:</label>
                <input type="text" class="input w-full border mt-2 flex-1" name="email" id="email"  aria-describedby="helpemail" value="{{$email}}" required>
                <span id="helpemail" class="help-block">
                    @if ($errors->has('email'))
                        @foreach($errors->get('email') as $message)
                            @if(!$loop->first) / @endif
                            {{ $message }}
                        @endforeach
                    @endif
                </span>
            </div>
            <div class="col-span-12 sm:col-span-12">
            </div>
        </div>
        <div class="px-5 py-3 text-right border-t border-gray-200">
            <button type="button" data-dismiss="modal" class="button w-20 border text-gray-700 mr-1">Cancelar</button>
            <button class="button w-20 bg-theme-1 text-white">Guardar</button>
        </div>
        {!! Form::close()!!}
    </div>
</div>


