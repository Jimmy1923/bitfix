<div class="modal" id="edit-usuario{{$id}}">
    <div class="modal__content modal__content--xl">
        <div class="flex items-center px-5 py-5 sm:py-3 border-b border-gray-200">
            <h2 class="font-medium text-base mr-auto">
                Edicion del usuario: {{$name}}
            </h2>
        </div>
        {!! Form::open(['route' => ['usuarios.update', $id], 'method' => 'PUT']) !!}
        {{ csrf_field() }}
        <input type="hidden" name="_method" value="PUT">
        <div class="p-5 grid grid-cols-12 gap-4 row-gap-4">
            <div class="col-span-12 sm:col-span-3 @if ($errors->has('name')) has-error @endif">
                <label class="font-medium text-base mr-auto" for="name">Nombre:</label>
                <input type="text" class="input w-full border mt-2 flex-1" name="name" id="name" value="{{$name}}" aria-describedby="helpnombre" required>
                <span id="helpnombre" class="help-block">
                    @if ($errors->has('name'))
                        @foreach($errors->get('name') as $message)
                            @if(!$loop->first) / @endif
                            {{ $message }}
                        @endforeach
                    @endif
                </span>
            </div>
            <div class="col-span-12 sm:col-span-4 @if ($errors->has('ap_paterno')) has-error @endif">
                <label class="font-medium text-base mr-auto" for="ap_paterno">Ap. paterno:</label>
                <input type="text" class="input w-full border mt-2 flex-1" name="ap_paterno" id="ap_paterno" aria-describedby="helpap_paterno" value="{{$ap_paterno}}" required>
                <span id="ap_paterno" class="help-block">
                    @if ($errors->has('helpap_paterno'))
                        @foreach($errors->get('ap_paterno') as $message)
                            @if(!$loop->first) / @endif
                            {{ $message }}
                        @endforeach
                    @endif
                </span>
            </div>
            <div class="col-span-12 sm:col-span-4 @if ($errors->has('ap_materno')) has-error @endif">
                <label class="font-medium text-base mr-auto" for="ap_materno">Ap. materno:</label>
                <input type="text" class="input w-full border mt-2 flex-1" name="ap_materno" id="ap_materno"  aria-describedby="helpap_materno" value="{{$ap_materno}}" required>
                <span id="helpap_materno" class="help-block">
                    @if ($errors->has('ap_materno'))
                        @foreach($errors->get('ap_materno') as $message)
                            @if(!$loop->first) / @endif
                            {{ $message }}
                        @endforeach
                    @endif
                </span>
            </div>
            <div class="col-span-12 sm:col-span-12">
            </div>
            <div class="col-span-12 sm:col-span-6 @if ($errors->has('email')) has-error @endif">
                <label class="font-medium text-base mr-auto" for="email">Email:</label>
                <input type="text" class="input w-full border mt-2 flex-1" name="email" id="email"  aria-describedby="helpemail" value="{{$email}}" required>
                <span id="helpemail" class="help-block">
                    @if ($errors->has('email'))
                        @foreach($errors->get('email') as $message)
                            @if(!$loop->first) / @endif
                            {{ $message }}
                        @endforeach
                    @endif
                </span>
            </div>
            <div class="col-span-12 sm:col-span-6 @if ($errors->has('role_id')) has-error @endif">
                <label class="font-medium text-base mr-auto" for="role_id">Rol:</label>
                <select class="input w-full border mt-2 flex-1" id="role_id" name="role_id">
                @php $rol = \App\Roles::all(); @endphp
                    @foreach($rol as $roles)
                    	<option value="{{$roles->id}}" @if($role_id == $roles->id) selected="selected" @endif>{{$roles->nombre}}</option>
                    @endforeach
                </select>
            </div>
            <div class="col-span-12 sm:col-span-12">
            </div>
        </div>
        <div class="px-5 py-3 text-right border-t border-gray-200">
            <button type="button" data-dismiss="modal" class="button w-20 border text-gray-700 mr-1">Cancelar</button>
            <button class="button w-20 bg-theme-1 text-white">Guardar</button>
        </div>
        {!! Form::close()!!}
    </div>
</div>


