@extends('layouts/app')
@section('title', 'Usuario')
@section('content')
<div class="intro-y flex flex-col sm:flex-row items-center mt-8">
    <h2 class="text-lg font-medium mr-auto">
        Usuarios
    </h2>
    <div class="w-full sm:w-auto flex mt-4 sm:mt-0">
        <a class="button text-white bg-theme-1 shadow-md mr-2" href="javascript:;" data-toggle="modal" data-target="#crear_usuario" >Crear usuario</a>
        @include('admin.usuario.create')
    </div>
</div><br><br>
 <div class="tab-content__pane active" id="usuarios">
	<div class="intro-y datatable-wrapper overflow-x-auto">
	    <table class="table table-report table-report--bordered display datatable w-full" id="usuario">
	        <thead>
	            <tr>
	                <th class="border-b-2 whitespace-no-wrap">Id</th>
                    <th class="border-b-2 whitespace-no-wrap">Nombre</th>
                    <th class="border-b-2 whitespace-no-wrap">Ap. paterno</th>
                    <th class="border-b-2 whitespace-no-wrap">Ap. materno</th>
	                <th class="border-b-2 text-center whitespace-no-wrap">Email</th>
	                <th class="border-b-2 text-center whitespace-no-wrap">Rol</th>
	                <th class="border-b-2 text-center whitespace-no-wrap">Proveedores creados</th>
	                <th class="border-b-2 text-center whitespace-no-wrap">Opciones</th>
	            </tr>
	        </thead>
	        <tbody>

	        </tbody>
	    </table>
	</div>
</div>
@endsection
@section('scripts_ready')
<script type="text/javascript">
    $(function () {
      $('#usuario').DataTable().destroy();
      var table = $('#usuario').DataTable({
          processing: true,
          serverSide: true,
        ajax: "{{ route('admin.listUser') }}",
        columns: [
            {data: 'id', name: 'id'},
            {data: 'name', name: 'name'},
            {data: 'ap_paterno', name: 'ap_paterno'},
            {data: 'ap_materno', name: 'ap_materno'},
            {data: 'email', name: 'email',class: "text-center border-b"},
            {data: 'role', name: 'role', class: "text-center border-b"},
            {data: 'proveedores', name: 'proveedores', class: "text-center border-b"},
            {data: 'btn', name: 'btn', class: "text-center border-b"},
        ],
            "language": {
                "info": "_TOTAL_ registros",
                "search": "Buscar",
                "paginate": {
                    "next": "Siguiente",
                    "previous": "Anterior",
                },
                "lengthMenu": 'Mostrar <select >'+
                            '<option value="10">10</option>'+
                            '<option value="30">30</option>'+
                            '<option value="-1">Todos</option>'+
                            '</select> registros',
                "loadingRecords": "Cargando...",
                "processing": "Procesando...",
                "emptyTable": "No hay datos",
                "zeroRecords": "No hay coincidencias",
                "infoEmpty": "",
                "infoFiltered": ""
            }
    });
  });
</script>
@endsection

