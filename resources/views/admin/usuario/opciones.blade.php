<div class="flex justify-center items-center">
    <a class="flex items-center text-theme-6" href="javascript:;" data-toggle="modal" data-target="#edit-usuario{{$id}}" value="{{$id}}"> <i data-feather="check-square" class="w-4 h-4 mr-1"></i> Editar </a>
    @include('admin.usuario.edit')
</div>
